// Selectors
var rectangle = document.querySelectorAll(".div1");
var jumboText = document.querySelector("h1");
var jumbotron = document.querySelector(".jumbotron");
var newGame = document.getElementsByClassName("new_game");


// Variables
var flagArray = [1,1,1,1,1,1];
var randomRGB = [0,0,0];
var correctAnswr = -1;
var stringRandomRGB = "RGB(0,0,0)"


// New Game button mechanism
function start () {
	for (var i=0; i<3; i++){
		randomRGB[i] = getRandomInt(256);
	}
	stringRandomRGB = "RGB(" + randomRGB[0]+ ", " + randomRGB[1] + ", " + randomRGB[2] + ")";
	
	jumboText.innerHTML =stringRandomRGB;
	flagArray = [1,1,1,1,1,1];

	correctAnswr = getRandomInt(6);

	for (var i=0; i<6; i++){
		var wrongRGB =[0,0,0]
		for (var j=0; j<3; j++){
		wrongRGB[j] = getRandomInt(256);
		}
		rectangle[i].style.background = "RGB(" + wrongRGB[0] + ", " + wrongRGB[1] + ", " + wrongRGB[2] + ")";
	}
	rectangle[correctAnswr].style.background = stringRandomRGB;
	jumbotron.style.background = "RGB(238, 238, 238)";
}

for (var i = 0; i < newGame.length; i++) {
    newGame[i].addEventListener("click", start);
}


// Color Buttons mechanism
function buttonOpacity(i){
	if (flagArray[i] === 1){
			flagArray[i] = 0;

			if (flagArray[correctAnswr] === 0){
				flagArray = [0,0,0,0,0,0];
				jumbotron.animate([{backgroundColor: "RGB(238,238,238)"},{backgroundColor: "RGB(255,255,255)"}], 800);
				
				for (var j=0; j<6; j++){
					rectangle[j].animate([{opacity: 1},{opacity: 0}],800);
				}

				setTimeout(function(){
					for (var i=0; i<6; i++){
						rectangle[i].style.background = "RGB(255,255,255)";
					}
					jumbotron.style.background = "RGB(255,255,255)";

					for (var k=0; k<6; k++){
						rectangle[k].animate([{backgroundColor: "RGB(255,255,255)"},{backgroundColor: stringRandomRGB}],800);
					}
					jumbotron.animate([{backgroundColor: "RGB(255,255,255)"},{backgroundColor: stringRandomRGB}], 800);
				},780);

				setTimeout(function(){
					for (var i=0; i<6; i++){
						rectangle[i].style.background = stringRandomRGB;
					}
					jumbotron.style.background = stringRandomRGB;
				},1580);

			} else {
				rectangle[i].animate([{opacity: 1},{opacity: 0}],500);
				//in future timeouts will be replaced by callback. But not now ;)
				setTimeout(function(){rectangle[i].style.background = "RGB(255,255,255)";}, 480);
			}
	}
}

//add listener to all rectangles
//ES5 version with IIFE
for (var i = 0; i < rectangle.length; i++) {
    rectangle[i].addEventListener("click", (function (i) {    	
    	return function() {
    		buttonOpacity(i)}
    	})(i)
    );
}
//in ES6 there will be just let i = 0 etc.

//first version
// rectangle[0].addEventListener("click", function() {buttonOpacity(0)});
// rectangle[1].addEventListener("click", function() {buttonOpacity(1)});
// rectangle[2].addEventListener("click", function() {buttonOpacity(2)});
// rectangle[3].addEventListener("click", function() {buttonOpacity(3)});
// rectangle[4].addEventListener("click", function() {buttonOpacity(4)});
// rectangle[5].addEventListener("click", function() {buttonOpacity(5)});


// Random number generator
function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}